<?php

/**
 * @file
 * Functions & hooks for addressfield_au module.
 */

define('ADDRESSFIELD_AU_BASE_TABLE', 'addressfield_au');

/**
 * Implements hook_ctools_plugin_directory().
 */
function addressfield_au_ctools_plugin_directory($module, $plugin) {
  if ($module == 'addressfield') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_menu().
 */
function addressfield_au_menu() {
  $items['addressfield-au/localities'] = array(
    'title' => 'localities autocomplete',
    'page callback' => 'addressfield_au_localities_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Default states in Australia.
 */
function addressfield_au_default_states() {
  $states = array(
    'ACT' => 'Australian Capital Territory',
    'NSW' => 'New South Wales',
    'NT' => 'Northern Territory',
    'QLD' => 'Queensland',
    'SA' => 'South Australia',
    'TAS' => 'Tasmania',
    'VIC' => 'Victoria',
    'WA' => 'Western Australia',
  );
  return $states;
}

/**
 * Save address in database.
 */
function addressefield_au_address_save($address) {
  if (empty($address['postal_code']) || !is_numeric($address['postal_code']) || empty('locality')) {
    return FALSE;
  }

  db_merge(ADDRESSFIELD_AU_BASE_TABLE)
    ->key(array(
      'postal_code' => $address['postal_code'],
      'locality' => $address['locality'],
    ))
    ->fields($address)
    ->execute();
}

/**
 * Get regions (sub_administrative_areas) from state (administrative_area) code.
 */
function addressfield_au_get_sub_administrative_areas($code) {
  $regions = array();
  $query = db_select(ADDRESSFIELD_AU_BASE_TABLE, 'au')
    ->fields('au', array('sub_administrative_area'))
    ->condition('administrative_area', $code)
    ->execute();
  while ($result = $query->fetchAssoc()) {
    $regions[$result['sub_administrative_area']] = $result['sub_administrative_area'];
  }
  return $regions;
}

/**
 * Get cities / towns/ suburbs from state code & region.
 */
function addressfield_au_get_localities($code, $sub_admin_area) {
  $localities = array();
  $query = db_select(ADDRESSFIELD_AU_BASE_TABLE, 'au')
    ->fields('au', array('locality'))
    ->condition('administrative_area', $code)
    ->condition('sub_administrative_area', $sub_admin_area)
    ->execute();
  while ($result = $query->fetchAssoc()) {
    $localities[$result['locality']] = $result['locality'];
  }
  return $localities;
}

/**
 * Get postal code from state code, region & city / town / suburb.
 */
function addressfield_au_get_postal_code($code, $sub_admin_area, $locality) {
  // There might be two entries for same state, region & city / town / suburb.
  // E.g. In Victoria, Melbourne region and Melbourne city there are two
  // postal codes. But as we are just using the for autofilling postal code
  // field, it is fine just to fetch one postal code & display that. User can
  // change it if needed.
  $query = db_select(ADDRESSFIELD_AU_BASE_TABLE, 'au')
    ->fields('au', array('postal_code'))
    ->condition('administrative_area', $code)
    ->condition('sub_administrative_area', $sub_admin_area)
    ->condition('locality', $locality)
    ->range(0, 1)
    ->execute();
  $postal_code = $query->fetchField();
  return $postal_code;
}

/**
 * Get address record using postal code & locality.
 */
function addressfield_au_get_address_by_postal_code_locality($postal_code = NULL, $locality = NULL) {
  if (is_null($postal_code) || is_null($locality)) {
    return FALSE;
  }

  $query = db_select(ADDRESSFIELD_AU_BASE_TABLE, 'au')
    ->fields('au')
    ->condition('postal_code', $postal_code)
    ->condition('locality', $locality)
    ->execute();
  if ($result = $query->fetchAssoc()) {
    return $result;
  }
  else {
    return FALSE;
  }
}

/**
 * Import CSV data of addresses.
 */
function _addressfield_au_import_csv_data() {
  $states = addressfield_au_default_states();
  foreach ($states as $code => $state_name) {
    // Fetch address data from csv for state.
    $address_data = _addressfield_au_fetch_address_from_csv($code);
    foreach ($address_data as $address) {
      $address['administrative_area'] = $code;
      addressefield_au_address_save($address);
    }
  }
}

/**
 * Fetch address data from csv for specific state.
 */
function _addressfield_au_fetch_address_from_csv($code) {
  $csv = drupal_get_path('module', 'addressfield_au') . '/csv/addressfield_' . $code . '.csv';
  $csv_lines = str_getcsv(file_get_contents($csv), "\n", '"');
  $address = array();
  foreach ($csv_lines as $line) {
    $line_array = explode(',', $line);
    $length = count($line_array) - 2;
    $localities = array_slice($line_array, 2, $length);
    foreach ($localities as $locality) {
      $element = array();
      $element['postal_code'] = $line_array[0];
      $element['sub_administrative_area'] = $line_array[1];
      $element['locality'] = trim($locality, '"');
      $element['locality'] = trim($element['locality']);
      $address[] = $element;
    }
  }
  return $address;
}

/**
 * Region autocomplete.
 */
function addressfield_au_localities_autocomplete($state, $sub_admin_area, $string = '') {
  $matches = array();
  if ($string) {
    $query = db_select('addressfield_au', 'au')
      ->fields('au', array('locality', 'sub_administrative_area', 'postal_code'))
      ->condition('locality', '%' . db_like($string) . '%', 'LIKE');
    if (!empty($state)) {
      $query->condition('administrative_area', $state);
    }
    if (!empty($sub_admin_area)) {
      $query->condition('sub_administrative_area', $sub_admin_area);
    }
    $query->range(0, 10);
    $result = $query->execute();
    while ($row = $result->fetchObject()) {
      $matches[$row->locality] = $row->locality;
    }
  }
  drupal_json_output($matches);
}
