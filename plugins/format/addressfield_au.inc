<?php

/**
 * @file
 * An addressfield formatter plugin for Australia.
 */

$plugin = array(
  'title' => t('Address form (Australia add-on)'),
  'format callback' => 'addressfield_format_address_au_generate',
  'type' => 'address',
  'weight' => -80,
);

/**
 * Default callback for addressfield plugin for Australia.
 */
function addressfield_format_address_au_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'AU' && $context['mode'] == 'form') {
    $id_count = &drupal_static('address_au_locality_id_counter', 1);
    $state_id = "address-au-state-$id_count";
    $sub_admin_area_id = "address-au-region-$id_count";
    $locality_html_id = "address-au-locality-$id_count";
    $postal_code_html_id = "address-au-postal-code-$id_count";
    $id_count++;

    // Build the options for states.
    $states = addressfield_au_default_states();
    $states = array_merge(array('' => t('Select State')), $states);

    $format['country']['#weight'] = 0;
    $format['locality_block']['#weight'] = 1;
    $format['street_block']['#weight'] = 2;

    // Extend the administrative area element.
    $element_administrative_area = array(
      '#options' => $states,
      '#ajax' => array(
        'callback' => 'addressfield_au_rebuild_sub_administrative_area',
        'wrapper' => $sub_admin_area_id,
        'method' => 'replace',
      ),
      '#prefix' => '<div id="' . $state_id . '">',
      '#suffix' => '</div>',
      '#weight' => 1,
      '#required' => TRUE,
    );

    // Merge overrides with original elements.
    $format['locality_block']['administrative_area'] = array_merge($format['locality_block']['administrative_area'], $element_administrative_area);

    // Get regions based on state.
    $regions = !empty($address['administrative_area']) ? addressfield_au_get_sub_administrative_areas($address['administrative_area']) : array();
    $regions = array_merge(array('' => t('Select Region')), $regions);
    // Should be disabled by default.
    $region_disabled = !empty($address['administrative_area']);
    // Create sub_administrative_areas elements as it is not available in
    // original module implementation.
    $format['locality_block']['sub_administrative_area'] = array(
      '#title' => t('Region'),
      '#type' => 'select',
      '#options' => $regions,
      '#prefix' => '<div id="' . $sub_admin_area_id . '">',
      '#suffix' => '</div>',
      '#ajax' => array(
        'callback' => 'addressfield_au_rebuild_locality',
        'wrapper' => $locality_html_id,
        'method' => 'replace',
      ),
      '#weight' => 2,
      '#disabled' => !$region_disabled,
    );

    // Prepare autocomplete path for localities based on previous elements.
    $autocomplete_path = 'addressfield-au/localities';
    if (!empty($address['administrative_area'])) {
      $autocomplete_path .= '/' . $address['administrative_area'];
    }
    if (!empty($address['sub_administrative_area'])) {
      $autocomplete_path .= '/' . $address['sub_administrative_area'];
    }
    $locality_disabled = !empty($address['sub_administrative_area']);
    // Extend locality element.
    $element_locality = array(
      '#title' => t('City / Town / Suburb'),
      '#autocomplete_path' => $autocomplete_path,
      // Add extra class for own js.
      '#attributes' => array('class' => array('addressfield-au-locality')),
      '#prefix' => '<div id="' . $locality_html_id . '">',
      '#suffix' => '</div>',
      '#size' => '20',
      '#weight' => 3,
      '#ajax' => array(
        'callback' => 'addressfield_au_rebuild_postal_code',
        'wrapper' => $postal_code_html_id,
        'method' => 'replace',
      ),
      '#disabled' => !$locality_disabled,
    );

    // Merge overrides with original elements.
    $format['locality_block']['locality'] = array_merge($format['locality_block']['locality'], $element_locality);

    // Extend postal code element.
    $postal_disabled = !empty($address['locality']);
    $element_postal_code = array(
      '#wrapper_id' => $format['#wrapper_id'],
      '#prefix' => '<div id="' . $postal_code_html_id . '">',
      '#suffix' => '</div>',
      '#weight' => 5,
      '#disabled' => !$postal_disabled,
    );

    $format['locality_block']['postal_code'] = array_merge($format['locality_block']['postal_code'], $element_postal_code);

    $format['locality_block']['dependent_locality']['#weight'] = 4;
  }
}

/**
 * Rebuild regions.
 */
function addressfield_au_rebuild_sub_administrative_area($form, $form_state) {
  $return = array();
  // $form is not completely built here.
  // The $sender['#parents'][0]][$sender['#parents'][1]][$sender['#parents'][2]
  // is the $form['field_name][language][delta] field form elemenent.
  $sender = $form_state['triggering_element'];
  $field_name = $sender['#parents'][0];
  $field_lang = $sender['#parents'][1];
  $field_delta = $sender['#parents'][2];

  // The addressfield values from $form_state.
  $field_element_values = $form_state['values'][$field_name][$field_lang][$field_delta];
  // The sub_administrative_area element within the $form array.
  $sub_administrative_area = $form[$field_name][$field_lang][$field_delta]['locality_block']['sub_administrative_area'];

  // Change the autocomplete path of locality field, and add the original ajax
  // wrapper.
  $code = !empty($field_element_values['administrative_area']) ? $field_element_values['administrative_area'] : NULL;

  if (!empty($code)) {
    $regions = addressfield_au_get_sub_administrative_areas($code);
    $regions = array_merge(array('' => t('Select Region')), $regions);
    $sub_administrative_area['#options'] = $regions;
    $sub_administrative_area['#disabled'] = FALSE;
  }

  return $sub_administrative_area;
}

/**
 * Rebuild locality.
 */
function addressfield_au_rebuild_locality($form, $form_state) {
  $return = array();
  // $form is not completely built here.
  // The $sender['#parents'][0]][$sender['#parents'][1]][$sender['#parents'][2]
  // is the $form['field_name][language][delta] field form elemenent.
  $sender = $form_state['triggering_element'];
  $field_name = $sender['#parents'][0];
  $field_lang = $sender['#parents'][1];
  $field_delta = $sender['#parents'][2];

  // The addressfield values from $form_state.
  $field_element_values = $form_state['values'][$field_name][$field_lang][$field_delta];
  // The locality element within the $form array.
  $locality = $form[$field_name][$field_lang][$field_delta]['locality_block']['locality'];

  $code = !empty($field_element_values['administrative_area']) ? $field_element_values['administrative_area'] : NULL;

  $sub_admin_area = !empty($field_element_values['sub_administrative_area']) ? $field_element_values['sub_administrative_area'] : NULL;

  if (!empty($code) && !empty($sub_admin_area)) {
    $locality['#autocomplete_path'] = 'addressfield-au/localities/' . $code . '/' . $sub_admin_area;
    $locality['#disabled'] = FALSE;
  }
  return $locality;
}

/**
 * Rebuild postal code.
 */
function addressfield_au_rebuild_postal_code($form, $form_state) {
  $return = array();
  // $form is not completely built here.
  // The $sender['#parents'][0]][$sender['#parents'][1]][$sender['#parents'][2]
  // is the $form['field_name][language][delta] field form elemenent.
  $sender = $form_state['triggering_element'];
  $field_name = $sender['#parents'][0];
  $field_lang = $sender['#parents'][1];
  $field_delta = $sender['#parents'][2];

  // The addressfield values from $form_state.
  $field_element_values = $form_state['values'][$field_name][$field_lang][$field_delta];
  // The postal_code element within the $form array.
  $postal_code = $form[$field_name][$field_lang][$field_delta]['locality_block']['postal_code'];

  $code = !empty($field_element_values['administrative_area']) ? $field_element_values['administrative_area'] : NULL;

  $sub_admin_area = !empty($field_element_values['sub_administrative_area']) ? $field_element_values['sub_administrative_area'] : NULL;

  $locality = !empty($field_element_values['locality']) ? $field_element_values['locality'] : NULL;

  if (!empty($code) && !empty($sub_admin_area) && !empty($locality)) {
    $post_code = addressfield_au_get_postal_code($code, $sub_admin_area, $locality);
    $postal_code['#default_value'] = $post_code;
    $postal_code['#attributes']['value'] = $post_code;
    $postal_code['#disabled'] = FALSE;
  }

  return $postal_code;
}
